#! /usr/bin/python
# -*-coding:Utf-8 -*
"""
Convert umi_tools generated whitelist or single column expected barcode cell
list into cutadapt expected tag file.
"""
import sys

def list_barcode_cell(file_in):
    """
    creates a list of cells barcodes
    """
    cells_barcodes = {}
    for line in file_in:
        barcode_cell_list = []
        table = line.split("\t")
        main_barcode = table[0].strip("\n")
        barcode_cell_list.append(main_barcode)
        cell_name = "Cell" + str(main_barcode)
        cells_barcodes[cell_name] = []
        if len(table) >= 2:
            mutated_barcodes = table[1].split(",")
            for barcode in mutated_barcodes:
                barcode_cell_list.append(barcode.strip("\n"))
        for elt in barcode_cell_list:
            cells_barcodes[cell_name].append(elt)
    return cells_barcodes

def reformat_list(file_out, cells_barcodes):
    """
    write list in output file in cutadapt expected format
    """
    for cell_name in cells_barcodes:
        for barcode in cells_barcodes[cell_name]:
            file_out.write(">"+str(cell_name)+"\n")
            file_out.write("^"+str(barcode)+"\n")

def main():
    """
    main
    """
    with open(sys.argv[1], "r") as file_in:
        with open(sys.argv[2], "w") as file_out:
            cells_barcodes = list_barcode_cell(file_in)
            reformat_list(file_out, cells_barcodes)




if __name__ == '__main__':
    main()
