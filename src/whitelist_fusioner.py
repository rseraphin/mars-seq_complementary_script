#! /usr/bin/python
# -*-coding:Utf-8 -*
"""
Fuse umi_tools generated whitelist into one with no duplicate
"""
import sys
import getopt


def usage():
    """
    print help
    """
    print("\nUsage for this tool\n")
    print("""-h / --help: print help,
    -i/ --input: input file,
    -o/ --output: output file""")

def list_barcode_cell(whitelist_file, cells_barcodes_list):
    """
    updates a dict of principal cells barcodes with a list of error containing
    barcodes as value
    """
    for line in whitelist_file:
        errored_barcodes = set()
        table = line.split("\t")
        main_barcode = table[0].strip()
        if len(table) > 2:
            for elt in table[1].split(","):
                errored_barcodes.add(elt.strip())

        if main_barcode not in cells_barcodes_list:
            cells_barcodes_list[main_barcode] = errored_barcodes
        else:
            if len(errored_barcodes) >= 1:
                cells_barcodes_list[main_barcode].update(errored_barcodes)
    return cells_barcodes_list

def write_whitelist(file_out, cells_barcodes_list):
    """
    writes the fused whitelist
    """
    for main_barcode in cells_barcodes_list:
        line = write_line(main_barcode, cells_barcodes_list[main_barcode])
        file_out.write(line)
        """file_out.write(main_barcode)
        if len(cells_barcodes_list[main_barcode]) >=1:
            file_out.write("\t")
        for errored_barcode in cells_barcodes_list[main_barcode]:
            file_out.write(errored_barcode+",")
        file_out.write("\n")
        """

def write_line(main_barcode, errored_barcodes):
    """
    defines a line to write
    """
    i = 0
    line = str(main_barcode)
    if len(errored_barcodes) >= 1:
        line += "\t"
        for barcode in errored_barcodes:
            i += 1
            if i == len(errored_barcodes):
                line += str(barcode)
            else:
                line += str(barcode)+","
    line += "\n"
    return line


def main():
    """
    main
    """
    try:
        opts, args = getopt.getopt(
            sys.argv[1:],
            "hvi:o:l:e:",
            ["help",
             "verbose",
             "input=",
             "output="])
    except getopt.GetoptError as err:
        # print help information and exit:
        print(str(err))  # will print something like "option -a not recognized"
        usage()
        sys.exit(2)
    for opt, arg in opts:
        if opt in ('-v', '--verbose'):
            print(opts, args)
        if opt in ('-o', '--output'):
            output_filename = arg
        elif opt in ('-i', '--input'):
            input_filename_list = arg
        elif opt in ('-h', '--help'):
            usage()
            sys.exit(2)

    cells_barcodes_list = {}
    with open(output_filename, "w") as file_out:
        list_input_file = input_filename_list.split(" ")
        for input_filename in list_input_file:
            with open(input_filename, "r") as whitelist_file:
                cells_barcodes_list = list_barcode_cell(whitelist_file,
                                                        cells_barcodes_list)
        write_whitelist(file_out, cells_barcodes_list)


if __name__ == "__main__":
    main()
