#! /usr/bin/env python3
# -*-coding:Utf-8 -*
"""
creates a matrix of barcodes cell counts
"""
import sys
import gzip


def set_dict(file_in):
    """
    creates a dict with barcodes as key and counts as values
    """
    dict_count = {}
    i = 0
    for bytes_line in file_in:
        line = bytes_line.decode()
        i += 1
        if (i//2)%2 != 0 and i%2 == 0:
            if line[0:6] not in dict_count:
                dict_count[line[0:6]] = 1
            else:
                dict_count[line[0:6]] += 1
    return dict_count

def sort_dict(dict_count):
    """
    sort the dict return a sorted list of tuples (key,value)
    """
    sorted_dict = sorted(dict_count.items(), key=lambda kv: kv[1], reverse=True)
    return sorted_dict

def write_dict(file_out, dict_count):
    """
    write out the dict in proper format
    """
    for elt in dict_count:
        file_out.write(str(elt[0])+"\t"+str(elt[1])+"\n")

def main():
    """
    main
    """
    with gzip.open(sys.argv[1], "rb") as file_in:
        dict_count = set_dict(file_in)
        sorted_dict = sort_dict(dict_count)
        with open(sys.argv[2], "w") as file_out:
            write_dict(file_out, sorted_dict)

if __name__ == '__main__':
    main()
